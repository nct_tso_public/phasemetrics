import os
import numpy as np
import csv

CHECK_VIDEO_LENGTHS = False
# optional -- uncomment if you want to run get_targets with check_video_lengths=True
# import cv2
# CHECK_VIDEO_LENGTHS = True


class EvalData:
    video_fps = None
    video_file_extension = None

    num_phases = None
    phase_labels = None

    @classmethod
    def get_datasplit(cls, datasplit):
        raise NotImplementedError

    @classmethod
    def get_targets(cls, video_ids, data_root, fps=1, check_video_lengths=CHECK_VIDEO_LENGTHS):
        raise NotImplementedError

    @staticmethod
    def get_dataset(name):
        if name.lower() == "cholec80":
            return Cholec80
        elif name.lower() == "autolaparo":
            return AutoLaparo
        elif name.lower().replace('-', '') == "sarrarp50":
            return SAR_RARP50
        else:
            raise ValueError("Unknown evaluation dataset '{}'.".format(name))


class Cholec80(EvalData):
    video_fps = 25
    video_file_extension = ".mp4"
    num_phases = 7
    phase_labels = list(range(num_phases))

    phase_map = dict()
    phase_map['Preparation'] = 0
    phase_map['CalotTriangleDissection'] = 1
    phase_map['ClippingCutting'] = 2
    phase_map['GallbladderDissection'] = 3
    phase_map['GallbladderPackaging'] = 4
    phase_map['CleaningCoagulation'] = 5
    phase_map['GallbladderRetraction'] = 6

    @classmethod
    def get_datasplit(cls, datasplit):
        if datasplit == "40-40":
            split = {
                'train': ["{:02d}".format(i) for i in range(1, 41)],
                'val': [],
                'test': ["{:02d}".format(i) for i in range(41, 81)]
            }
        elif datasplit == "32-8-40":
            split = {
                'train': ["{:02d}".format(i) for i in range(1, 33)],
                'val': ["{:02d}".format(i) for i in range(33, 41)],
                'test': ["{:02d}".format(i) for i in range(41, 81)]
            }
        elif datasplit == "40-8-32":
            split = {
                'train': ["{:02d}".format(i) for i in range(1, 41)],
                'val': ["{:02d}".format(i) for i in range(41, 49)],
                'test': ["{:02d}".format(i) for i in range(49, 81)]
            }
        elif datasplit == "40-20-20":
            split = {
                'train': ["{:02d}".format(i) for i in range(1, 41)],
                'val': ["{:02d}".format(i) for i in range(41, 61)],
                'test': ["{:02d}".format(i) for i in range(61, 81)]
            }
        else:
            raise ValueError("Unknown datasplit '{}'".format(datasplit))

        return split

    @classmethod
    def get_targets(cls, video_ids, data_root, fps=1, check_video_lengths=CHECK_VIDEO_LENGTHS):
        assert (fps <= cls.video_fps and cls.video_fps % fps == 0)
        video_step = cls.video_fps // fps

        targets = dict()
        for video_id in video_ids:
            anno_labels = []
            f = open(os.path.join(data_root, "phase_annotations", "video{}-phase.txt".format(video_id)), "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)  # skip first row
            for row in reader:
                anno_labels.append(cls.phase_map[row[1]])
            f.close()

            if check_video_lengths:
                video_file = "video{}".format(video_id) + cls.video_file_extension
                cap = cv2.VideoCapture(os.path.join(data_root, "videos", video_file))
                video_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
                if video_frames < len(anno_labels):
                    print("video {}: found annotations for {} frames, but video contains only {} frames!".
                          format(video_id, len(anno_labels), video_frames))
                    anno_labels = anno_labels[:video_frames]

            targets[video_id] = anno_labels[::video_step]

        return targets


class AutoLaparo(EvalData):
    video_fps = 25
    video_file_extension = ".mp4"
    num_phases = 7
    phase_labels = list(range(num_phases))

    phase_map = dict()
    phase_map['1'] = 0
    phase_map['2'] = 1
    phase_map['3'] = 2
    phase_map['4'] = 3
    phase_map['5'] = 4
    phase_map['6'] = 5
    phase_map['7'] = 6

    @classmethod
    def get_datasplit(cls, datasplit):
        if datasplit == "10-4-7":
            split = {
                'train': ["{:02d}".format(i) for i in range(1, 11)],
                'val': ["{:02d}".format(i) for i in range(11, 15)],
                'test': ["{:02d}".format(i) for i in range(15, 22)]
            }
        elif datasplit == "14-7":
            split = {
                'train': ["{:02d}".format(i) for i in range(1, 15)],
                'val': [],
                'test': ["{:02d}".format(i) for i in range(15, 22)]
            }
        else:
            raise ValueError("Unknown datasplit '{}'".format(datasplit))

        return split

    @classmethod
    def get_targets(cls, video_ids, data_root, fps=1, check_video_lengths=CHECK_VIDEO_LENGTHS):
        assert (fps == 1), "Phase annotations available only at 1 fps"

        targets = dict()
        for video_id in video_ids:
            anno_labels = []
            f = open(os.path.join(data_root, "labels", "label_{}.txt".format(video_id)), "r")
            reader = csv.reader(f, delimiter='\t')
            next(reader, None)  # skip first row
            for row in reader:
                anno_labels.append(cls.phase_map[row[1]])
            f.close()

            if check_video_lengths:
                video_file = "{}".format(video_id) + cls.video_file_extension
                cap = cv2.VideoCapture(os.path.join(data_root, "videos", video_file))
                video_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
                annotated_frames = len(anno_labels) * cls.video_fps
                if video_frames < annotated_frames and (abs(video_frames - annotated_frames) >= cls.video_fps):
                    print("video {}: found annotations for {} frames, but video contains only {} frames!".
                          format(video_id, annotated_frames, video_frames))
                    anno_labels = anno_labels[:(video_frames // cls.video_fps)]

            targets[video_id] = anno_labels

        return targets


class SAR_RARP50(EvalData):
    video_fps = 60
    anno_fps = 10
    num_phases = 8
    phase_labels = list(range(num_phases))

    phase_map = dict()
    phase_map['0'] = 0
    phase_map['1'] = 1
    phase_map['2'] = 2
    phase_map['3'] = 3
    phase_map['4'] = 4
    phase_map['5'] = 5
    phase_map['6'] = 6
    phase_map['7'] = 7

    @classmethod
    def get_datasplit(cls, datasplit):
        if datasplit == "35-5-10":
            split = {
                'train': ["{:02d}".format(i) for i in range(1, 36)],
                'val': ["{:02d}".format(i) for i in range(36, 41)],
                'test': ["{:02d}".format(i) for i in range(41, 51)]
            }
        else:
            raise ValueError("Unknown datasplit '{}'".format(datasplit))

        return split

    @classmethod
    def get_targets(cls, video_ids, data_root, fps=10, check_video_lengths=CHECK_VIDEO_LENGTHS):
        assert (fps <= cls.anno_fps and cls.anno_fps % fps == 0)
        anno_step = cls.anno_fps // fps

        targets = dict()
        for video_id in video_ids:
            anno_labels = []
            f = open(os.path.join(data_root, "video_{}".format(video_id), "action_discrete.txt"), "r")
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                anno_labels.append(cls.phase_map[row[1]])
            f.close()

            if check_video_lengths:
                cap = cv2.VideoCapture(os.path.join(data_root, "video_{}".format(video_id), "video_left.avi"))
                video_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
                anno_factor = int(cls.video_fps // cls.anno_fps)
                annotated_frames = len(anno_labels) * anno_factor
                if video_frames < annotated_frames and (abs(video_frames - annotated_frames) >= anno_factor):
                    print("video {}: found annotations for {} frames, but video contains only {} frames!".
                          format(video_id, annotated_frames, video_frames))
                    anno_labels = anno_labels[:(video_frames // anno_factor)]

            targets[video_id] = anno_labels[::anno_step]

        return targets


def get_phase_segments(y_true):
    """ Compute sequence of phases and their start/end times in video annotation 'y_true'."""
    if isinstance(y_true, list):
        y_true = np.asarray(y_true)
    assert (y_true.ndim == 1)
    phase_labels = []
    phase_borders = []
    current_phase = -1
    for i in range(len(y_true)):
        phase = y_true[i]
        if phase != current_phase:  # beginning of next phase
            phase_labels.append(phase)
            phase_borders.append(i)
            current_phase = phase
    phase_borders.append(i + 1)

    return phase_labels, phase_borders