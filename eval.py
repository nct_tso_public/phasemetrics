import argparse
import os
import numpy as np
import yaml
import warnings

from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, jaccard_score, \
    confusion_matrix, balanced_accuracy_score

from .edit_score import calc_edit_score
from .overlap_f1 import calc_overlap_f1
from .utils import EvalData, get_phase_segments

# set OCTAVE_CHECK = False to avoid looking for Octave
OCTAVE_CHECK = True
if OCTAVE_CHECK is True:
    OCTAVE_PATH = "PhaseMetrics/matlab-eval"
    OCTAVE_FOUND = True
    try:
        from oct2py import octave
        octave.eval('disp("Octave found.")')
        print("Looking for MATLAB Evaluate function...")
        octave.addpath(OCTAVE_PATH)
        octave.eval('help Evaluate')
    except:
        print("Error importing oct2py.octave... Will not be able to execute MATLAB evaluation code!")
        OCTAVE_FOUND = False
else:
    OCTAVE_FOUND = False

eps = 0.000001


def report_results(experiment, datasplit, data_root, results_root="PhaseMetrics/results", run=None,
                   out_dir=None, force_overwrite=False, fps=1, eval_data="Cholec80"):
    """ Generate evaluation report.

    Predictions are expected to be found in 'predictions.yaml' files,
    located at <results_root>/<experiment>/<datasplit>/<run>/.

    If run is None, the predictions in all subdirectories of <results_root>/<experiment>/<datasplit> are evaluated
    and the results are summarized over all of these runs.

    Evaluation results are summarized by reporting mean, standard deviation over videos, standard deviation over phases,
    if applicable, and standard deviation over experimental runs, if run is None and there is more than one
    subdirectory in <results_root>/<experiment>/<datasplit>.

    The summarized evaluation results are stored in a yaml-file, eval.yaml.

    Parameters
    ----------
    experiment: str
        The experiment to evaluate.
    datasplit: str
        The data split to use.
    data_root: str
        Where to look for the evaluation data.
    results_root: str, optional
        Where to look for the predictions.
    run: str, optional
        The experimental run to evaluate.
    out_dir: str, optional
        Where to store the evaluation report. By default, it will be stored at <results_root>/<experiment>/<datasplit>/
        if run is None, otherwise at <results_root>/<experiment>/<datasplit>/<run>/
    force_overwrite: str, optional
        If True, an existing yaml-file, if stored at the intended yaml-file location, will be overwritten.
        Otherwise, the existing file will be kept and the results stored in the file will be read and returned,
        without recomputing anything.
    fps: int, optional
        The frequency (in frames per second) at which predictions were made. Defaults to 1.
    eval_data: str, optional
        Specifies which dataset is used for evaluation. Valid choices are Cholec80, AutoLaparo, and SAR-RARP50.
        Defaults to Cholec80.

    Returns
    -------
    dict
        A dictionary of summary evaluation results.
    """

    if out_dir is not None:
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        out_file = os.path.join(out_dir, "eval.yaml")
    else:
        if run is not None:
            out_file = os.path.join(results_root, experiment, datasplit, run, "eval.yaml")
        else:
            out_file = os.path.join(results_root, experiment, datasplit, "eval.yaml")
    if os.path.exists(out_file):
        if force_overwrite:
            print("Overwriting evaluation report at '{}'.".format(out_file))
            os.remove(out_file)
        else:
            print("Found evaluation report at '{}'.".format(out_file))
            report = yaml.safe_load(open(out_file))
            return report

    eval_dataset = EvalData.get_dataset(eval_data)
    nlabels = eval_dataset.num_phases
    test_ids = eval_dataset.get_datasplit(datasplit)['test']
    experiment_results = get_results(experiment, datasplit, data_root, results_root, run, fps, eval_data=eval_data)
    nruns = len(experiment_results)
    assert (nruns > 0)

    print("Creating report...")
    report = {
        "predictions_dir": str(os.path.join(results_root, experiment, datasplit)),
        "run_dirs": ["{}".format(t[1]) for t in
                     sorted([experiment_results[i]["run"] for i in range(nruns)], key=lambda t: t[0])
                     ],
        "fps": fps,
    }
    for metric in ["accuracy", "balanced_accuracy", "edit_score"] + \
                  ['overlap_f1_{}'.format(overlap) for overlap in [10, 25, 50, 75, 90]]:
        report[metric] = summarize_videowise_metric(experiment_results, test_ids, nruns, metric)
    for metric in ["macro_precision", "macro_recall", "macro_f1", "macro_jaccard", "inflated_macro_f1"]:
        report[metric] = dict()
        for nan_strategy in ["A", "B"]:
            report[metric][nan_strategy] = \
                summarize_videowise_metric(experiment_results, test_ids, nruns, metric, nan_strategy)

    for metric in ["precision", "recall", "f1", "jaccard"]:
        report[metric] = dict()
        for nan_strategy in ["A", "B"]:
            report[metric][nan_strategy] =  \
                summarize_phasewise_videowise_metric(experiment_results, test_ids, nruns, metric, nlabels, nan_strategy)

    report["inflated_macro_f1_upper_bound"] = dict()
    for nan_strategy in ["A", "B"]:
        report["inflated_macro_f1_upper_bound"][nan_strategy] = \
            calculate_f1_upper_bound(experiment_results, test_ids, nruns, nlabels, nan_strategy)

    for metric in ["accuracy", "precision", "recall", "f1", "jaccard"]:
        report["framewise_{}".format(metric)] = summarize_framewise_metric(experiment_results, nruns, metric, nlabels)

    if eval_data.lower() == "cholec80":
        report.update(
            summarize_relaxed_metrics(experiment_results, test_ids, nruns, nlabels, compare_to_orig=OCTAVE_FOUND is True)
        )

    yaml.safe_dump(report, stream=open(out_file, 'w'), default_flow_style=False)
    print("Stored evaluation report at '{}'.".format(out_file))

    return report


def summarize_videowise_metric(experiment_results, test_ids, nruns, metric, nan_strategy=None):
    if nan_strategy is None:
        all_results = np.asarray(
            [[experiment_results[run][video_id][metric] for video_id in test_ids]
             for run in range(nruns)]
        )
    else:
        all_results = np.asarray(
            [[experiment_results[run][video_id][metric][nan_strategy] for video_id in test_ids]
             for run in range(nruns)]
        )
    assert (all_results.ndim == 2 and all_results.shape[0] == nruns and all_results.shape[1] == len(test_ids))

    return {
        'mean': np.mean(all_results).item(),
        'std_V': np.std(np.mean(all_results, axis=0), ddof=1).item(),
        'std_R': np.std(np.mean(all_results, axis=1), ddof=1).item() if nruns > 1 else np.nan
    }


def summarize_phasewise_videowise_metric(experiment_results, test_ids, nruns, metric, nlabels, nan_strategy=None):
    result = dict()

    if nan_strategy is None:
        all_results = np.asarray(
            [[experiment_results[run][video_id][metric] for video_id in test_ids]
             for run in range(nruns)])
    else:
        all_results = np.asarray(
            [[experiment_results[run][video_id][metric][nan_strategy] for video_id in test_ids]
             for run in range(nruns)])
    assert (all_results.ndim == 3 and all_results.shape[0] == nruns and all_results.shape[1] == len(test_ids)
            and all_results.shape[2] == nlabels)

    # overall results

    result['mean'] = np.nanmean(all_results).item()
    result['std_V'] = np.std(np.nanmean(all_results, axis=(0, 2)), ddof=1).item()
    result['std_P'] = np.std(np.nanmean(all_results, axis=(0, 1)), ddof=1).item()
    result['std_R'] = np.std(np.nanmean(all_results, axis=(1, 2)), ddof=1).item()

    # phasewise results

    for phase in range(nlabels):
        phase_results = all_results[:, :, phase]
        assert (phase_results.ndim == 2 and phase_results.shape[0] == nruns and phase_results.shape[1] == len(test_ids))

        # catch corner cases where one phase is present in only a few test videos
        mean_per_video = np.nanmean(phase_results, axis=0)
        n_valid = np.sum(~np.isnan(mean_per_video))
        if n_valid == 0:
            _std_V_ = np.nan
        elif n_valid == 1:
            _std_V_ = 0
        else:
            _std_V_ = np.nanstd(mean_per_video, ddof=1).item()

        result[phase] = {
            'mean': np.nanmean(phase_results).item(),
            'std_V': _std_V_,
            'std_R': np.nanstd(np.nanmean(phase_results, axis=1), ddof=1).item() if nruns > 1 else np.nan
        }

    std_P_ = np.std([result[phase]['mean'] for phase in range(nlabels)], ddof=1).item()
    if abs(result['std_P'] - std_P_) >= eps:
        warnings.warn("{}[{}]: Unexpectedly high deviation ({}) between std_P ({}) and std of phase-wise means ({}).".
                      format(metric, nan_strategy, eps, result['std_P'], std_P_))

    result['mean_P'] = np.mean([result[phase]['mean'] for phase in range(nlabels)]).item()

    return result


def calculate_f1_upper_bound(experiment_results, test_ids, nruns, nlabels, nan_strategy=None):
    result = dict()

    precision_scores = np.asarray(
        [[experiment_results[run][video_id]["precision"][nan_strategy] for video_id in test_ids] for run in range(nruns)]
    )
    assert (precision_scores.ndim == 3 and precision_scores.shape[0] == nruns
            and precision_scores.shape[1] == len(test_ids) and precision_scores.shape[2] == nlabels)
    mean_precision_per_run = np.nanmean(precision_scores, axis=(1, 2))

    recall_scores = np.asarray(
        [[experiment_results[run][video_id]["recall"][nan_strategy] for video_id in test_ids] for run in range(nruns)]
    )
    assert (recall_scores.ndim == 3 and recall_scores.shape[0] == nruns and recall_scores.shape[1] == len(test_ids)
            and recall_scores.shape[2] == nlabels)
    mean_recall_per_run = np.nanmean(recall_scores, axis=(1, 2))

    f1_upper_bound_per_run = [
        (2 * mean_precision_per_run[i] * mean_recall_per_run[i]) / (mean_precision_per_run[i] + mean_recall_per_run[i])
        for i in range(nruns)
    ]
    result['mean'] = np.mean(f1_upper_bound_per_run).item()
    result['std_R'] = np.std(f1_upper_bound_per_run, ddof=1).item() if nruns > 1 else np.nan

    if nruns > 1:  # add harmonic mean of overall means
        mean_precision = np.nanmean(precision_scores)
        mean_recall = np.nanmean(recall_scores)
        result['overall'] = ((2 * mean_precision * mean_recall) / (mean_precision + mean_recall)).item()

    return result


def summarize_framewise_metric(experiment_results, nruns, metric, nlabels):
    result = dict()
    if metric != "accuracy":
        all_results = np.asarray([experiment_results[run]["framewise_{}".format(metric)] for run in range(nruns)])
        assert (all_results.ndim == 2 and all_results.shape[0] == nruns and all_results.shape[1] == nlabels)

        if metric == 'precision':
            # NaN values can occur in case that a phase is not predicted in any of the videos
            result['mean'] = np.nanmean(all_results).item()
            result['std_P'] = np.std(np.nanmean(all_results, axis=0), ddof=1).item()
            result['std_R'] = np.std(np.nanmean(all_results, axis=1), ddof=1).item() if nruns > 1 else np.nan

            for phase in range(nlabels):
                _results = all_results[:, phase]
                result[phase] = {
                    'mean': np.nanmean(_results).item(),
                    'std_R': np.nanstd(_results, ddof=1).item() if nruns > 1 else np.nan
                }
        else:
            result['mean'] = np.mean(all_results).item()
            result['std_P'] = np.std(np.mean(all_results, axis=0), ddof=1).item()
            result['std_R'] = np.std(np.mean(all_results, axis=1), ddof=1).item() if nruns > 1 else np.nan

            for phase in range(nlabels):
                _results = all_results[:, phase]
                result[phase] = {
                    'mean': np.mean(_results).item(),
                    'std_R': np.std(_results, ddof=1).item() if nruns > 1 else np.nan
                }
    else:  # metric == "accuracy"
        all_results = np.asarray([experiment_results[run]["framewise_accuracy"] for run in range(nruns)])
        assert (all_results.ndim == 1 and all_results.shape[0] == nruns)

        result['mean'] = np.mean(all_results).item()
        result['std_R'] = np.std(all_results, ddof=1).item() if nruns > 1 else np.nan

    return result


def summarize_relaxed_metrics(experiment_results, test_ids, nruns, nlabels, compare_to_orig=True):
    if compare_to_orig is True:
        if nruns > 1:
            print("Summarization of relaxed metrics was implemented for one run only. "
                  "Cannot compare when summarizing more than one run.")
            compare_to_orig = False
        else:
            octave.addpath(OCTAVE_PATH)

    result = dict()

    for metric in ["relaxed_precision", "relaxed_recall", "relaxed_jaccard"]:
        all_results = np.asarray(
            [[experiment_results[run][video_id][metric] for video_id in test_ids]
             for run in range(nruns)])
        assert (all_results.ndim == 3 and all_results.shape[0] == nruns and all_results.shape[1] == len(test_ids)
                and all_results.shape[2] == nlabels)

        result[metric] = dict()
        for phase in range(nlabels):
            phase_results = all_results[:, :, phase]
            result[metric][phase] = {
                'mean': np.nanmean(phase_results).item(),
                'std_V': np.nanstd(np.nanmean(phase_results, axis=0), ddof=1).item(),
                'std_R': np.std(np.nanmean(phase_results, axis=1), ddof=1).item() if nruns > 1 else np.nan
            }

        result[metric]['mean_P'] = np.mean([result[metric][phase]['mean'] for phase in range(nlabels)]).item()
        result[metric]['std_P'] = np.std(np.nanmean(all_results, axis=(0, 1)), ddof=1).item()

    metric = "relaxed_accuracy"
    all_results = np.asarray(
        [[experiment_results[run][video_id][metric] for video_id in test_ids] for run in range(nruns)]
    )
    assert (all_results.ndim == 2 and all_results.shape[0] == nruns and all_results.shape[1] == len(test_ids))

    result[metric] = {
        'mean': np.mean(all_results).item(),
        'std_V': np.std(np.mean(all_results, axis=0), ddof=1).item(),
        'std_R': np.std(np.mean(all_results, axis=1), ddof=1).item() if nruns > 1 else np.nan
    }

    if compare_to_orig is True:  # compare to original matlab implementation
        jaccard_in = np.asarray([experiment_results[0][video_id]["relaxed_jaccard"] for video_id in test_ids]).transpose()
        precision_in = np.asarray([experiment_results[0][video_id]["relaxed_precision"] for video_id in test_ids]).transpose()
        recall_in = np.asarray([experiment_results[0][video_id]["relaxed_recall"] for video_id in test_ids]).transpose()
        for mat in [jaccard_in, precision_in, recall_in]:
            assert (mat.ndim == 2 and mat.shape[0] == nlabels and mat.shape[1] == len(test_ids))
        accuracy_in = [experiment_results[0][video_id]["relaxed_accuracy"] for video_id in test_ids]

        meanJacc_orig, stdJacc_orig, meanAcc_orig, stdAcc_orig, meanPrec_orig, stdPrec_orig, meanRec_orig, stdRec_orig \
            = octave.feval('Summarize', accuracy_in, jaccard_in, precision_in, recall_in, nout=8)

        assert (abs(meanJacc_orig - result["relaxed_jaccard"]["mean_P"]) < eps)
        assert (abs(stdJacc_orig - result["relaxed_jaccard"]["std_P"]) < eps)
        assert (abs(meanPrec_orig - result["relaxed_precision"]["mean_P"]) < eps)
        assert (abs(stdPrec_orig - result["relaxed_precision"]["std_P"]) < eps)
        assert (abs(meanRec_orig - result["relaxed_recall"]["mean_P"]) < eps)
        assert (abs(stdRec_orig - result["relaxed_recall"]["std_P"]) < eps)
        assert (abs(meanAcc_orig - result["relaxed_accuracy"]["mean"]) < eps)
        assert (abs(stdAcc_orig - result["relaxed_accuracy"]["std_V"]) < eps)

    return result


def get_results(experiment, datasplit, data_root, results_root="PhaseMetrics/results", run=None, fps=1,
                return_conf_mat=False, eval_data="Cholec80"):
    """ Calculate video-wise and frame-wise evaluation results.

    Predictions are expected to be found in 'predictions.yaml' files,
    located at <results_root>/<experiment>/<datasplit>/<run>/.

    If run is None, the predictions in all subdirectories of <results_root>/<experiment>/<datasplit> are evaluated.

    Parameters
    ----------
    experiment: str
        The experiment to evaluate.
    datasplit: str
        The data split to use.
    data_root: str
        Where to look for the evaluation data.
    results_root: str, optional
        Where to look for the predictions.
    run: str, optional
        The experimental run to evaluate.
    fps: int, optional
        The frequency (in frames per second) at which predictions were made. Defaults to 1.
    return_conf_mat: bool, optional
        Whether or not to return the overall confusion matrix. Defaults to False.
    eval_data: str, optional
        Specifies which dataset is used for evaluation. Valid choices are Cholec80 and AutoLaparo. Defaults to Cholec80.

    Returns
    -------
    list or (list, np.ndarray)
        A list of evaluation results, with one item per experimental run.
        For each experimental run, a dictionary of video-wise and frame-wise evaluation results is stored in the list.
        If return_conf_mat is True, the overall confusion matrix, which is the sum of the frame-wise confusion matrices
        over all experimental runs, is returned in addition.
    """

    eval_dataset = EvalData.get_dataset(eval_data)
    compute_relaxed_metrics = eval_data.lower() == "cholec80"  # defined only for Cholec80
    nlabels = eval_dataset.num_phases
    test_ids = eval_dataset.get_datasplit(datasplit)['test']
    targets = eval_dataset.get_targets(test_ids, data_root, fps)

    predictions_dir = os.path.join(results_root, experiment, datasplit)
    if run is not None:
        assert (os.path.exists(os.path.join(predictions_dir, run)))
        run_dirs = [run]
    else:
        run_dirs = []
        for f in os.listdir(predictions_dir):
            if os.path.isdir(os.path.join(predictions_dir, f)) and \
                    os.path.exists(os.path.join(predictions_dir, f, "predictions.yaml")):
                run_dirs.append(f)
        run_dirs = sorted(run_dirs)
        if len(run_dirs) > 0:
            print("Found results for {} experiment repetitions in the sub-directories: {}".
                  format(len(run_dirs), run_dirs))
        else:
            print("Could not find any runs in '{}'.".format(predictions_dir))
            return []

    experiment_results = []
    if return_conf_mat is True:
        confusion_matrix = np.zeros([nlabels, nlabels], dtype=np.int64)
    cnt = 0
    for run_dir in run_dirs:
        predictions_file = os.path.join(predictions_dir, run_dir, "predictions.yaml")
        print("Computing metrics for file {}...".format(predictions_file))

        if return_conf_mat:
            run_results, conf_mat = get_results_(
                predictions_file, targets, test_ids, nlabels, fps, return_conf_mat=True,
                compute_relaxed_metrics=compute_relaxed_metrics
            )
            confusion_matrix += conf_mat
        else:
            run_results = get_results_(
                predictions_file, targets, test_ids, nlabels, fps, return_conf_mat=False,
                compute_relaxed_metrics=compute_relaxed_metrics
            )
        run_results["run"] = (cnt, run_dir)

        experiment_results.append(run_results)
        cnt += 1

    assert (cnt == len(experiment_results))

    if return_conf_mat:
        return experiment_results, confusion_matrix
    else:
        return experiment_results


def get_results_(predictions_file, targets, test_ids, nlabels, fps=1,
                 return_conf_mat=False, compute_relaxed_metrics=True):
    predictions_dict = yaml.safe_load(open(predictions_file))
    assert (predictions_dict['fps'] == fps)

    run_results = dict()
    framewise_confusion_matrix = np.zeros([nlabels, nlabels], dtype=np.int64)
    for video_id in test_ids:
        y_pred = np.asarray(predictions_dict[video_id])
        y_true = np.asarray(targets[video_id])

        if y_pred.shape[0] == (y_true.shape[0] - 1):  # off by one
            len_expected = y_true.shape[0]
            y_true = y_true[:-1]
            warnings.warn("Predictions for video {} have length {}, but expected length {}.".format(
                video_id, y_pred.shape[0], len_expected
            ))
        assert y_pred.shape[0] == y_true.shape[0], \
            "Cannot compare predictions for video {} with length {} to ground truth annotation with length {}!".format(
                video_id, y_pred.shape[0], y_true.shape[0]
            )

        run_results[video_id] = calc_video_metrics(y_true, y_pred, nlabels)
        framewise_confusion_matrix += run_results[video_id]["confusion_matrix"]

        if compute_relaxed_metrics is True:
            relaxed_metrics = calc_relaxed_video_metrics(y_true, y_pred, list(range(nlabels)), fps, w=10,
                                                         compare_to_orig=OCTAVE_FOUND is True,
                                                         reproduce_bug=True, clip_results=True)
            for metric in relaxed_metrics:
                run_results[video_id]["relaxed_{}".format(metric)] = relaxed_metrics[metric]

    framewise_metrics = calc_conf_mat_metrics(framewise_confusion_matrix, nlabels)
    for metric in framewise_metrics:
        run_results["framewise_{}".format(metric)] = framewise_metrics[metric]

    if return_conf_mat:
        return run_results, framewise_confusion_matrix
    else:
        return run_results


def calc_video_metrics(y_true, y_pred, nlabels):
    if (y_true.shape[0] != y_pred.shape[0]):
        raise RuntimeError("Incompatible shapes: y_true {}, y_pred {}".format(y_true.shape, y_pred.shape))

    metrics = {
        'accuracy': None,
        'balanced_accuracy': None,  # equivalent to macro recall
        'confusion_matrix': None,
        'precision': {},  # phase-wise scores
        'recall': {},
        'f1': {},
        'jaccard': {},
        'macro_precision': {},  # macro-averaged scores
        'macro_recall': {},
        'macro_f1': {},
        'macro_jaccard': {},
        'inflated_macro_f1': {},  # harmonic mean over arithmetic means (see https://arxiv.org/pdf/1911.03347.pdf)
        'edit_score': None,
    }

    labels = list(range(nlabels))

    metrics['accuracy'] = accuracy_score(y_true, y_pred)
    metrics['balanced_accuracy'] = balanced_accuracy_score(y_true, y_pred, adjusted=False)
    metrics['confusion_matrix'] = confusion_matrix(y_true, y_pred, labels=labels, normalize=None)

    for nan_strategy in ["A", "B"]:
        precision = calc_classification_scores(precision_score, y_true, y_pred, nlabels, nan_strategy=nan_strategy)
        macro_precision = np.nanmean(precision)
        recall = calc_classification_scores(recall_score, y_true, y_pred, nlabels, nan_strategy=nan_strategy)
        macro_recall = np.nanmean(recall)
        f1 = calc_classification_scores(f1_score, y_true, y_pred, nlabels, nan_strategy=nan_strategy)
        jaccard = calc_classification_scores(jaccard_score, y_true, y_pred, nlabels, nan_strategy=nan_strategy)

        metrics['precision'][nan_strategy] = precision
        metrics['macro_precision'][nan_strategy] = macro_precision
        metrics['recall'][nan_strategy] = recall
        metrics['macro_recall'][nan_strategy] = macro_recall
        metrics['f1'][nan_strategy] = f1
        metrics['macro_f1'][nan_strategy] = np.nanmean(f1)
        metrics['jaccard'][nan_strategy] = jaccard
        metrics['macro_jaccard'][nan_strategy] = np.nanmean(jaccard)

        if (macro_precision + macro_recall) == 0:
            metrics['inflated_macro_f1'][nan_strategy] = 0
        else:
            metrics['inflated_macro_f1'][nan_strategy] = \
                (2 * macro_precision * macro_recall) / (macro_precision + macro_recall)

    metrics['edit_score'] = calc_edit_score(y_true, y_pred)
    for overlap in [10, 25, 50, 75, 90]:
        metrics['overlap_f1_{}'.format(overlap)] = calc_overlap_f1(y_true, y_pred, nlabels, overlap=overlap / 100)

    return metrics


def calc_classification_scores(score_fn, y_true, y_pred, nlabels, nan_strategy="A"):
    """Calculate phase-wise classification scores with custom undefined value handling."""
    assert (score_fn in [precision_score, recall_score, f1_score, jaccard_score])
    assert (nan_strategy in ["A", "B", "C"])
    # A: undefined values will be NaN
    # B: set scores to NaN for phase labels that are missing in y_true
    # C: if phase label is missing in both y_true and y_pred, set score to 1 if score_fn in {Precision, F1, Jaccard}

    labels = list(range(nlabels))
    scores = score_fn(y_true, y_pred, labels=labels, average=None, zero_division=0)  # phase-wise scores
    assert (len(scores) == nlabels)

    # handle undefined values (NaNs)
    for label in range(nlabels):
        label_missing = np.sum(y_true == label) == 0
        label_not_predicted = np.sum(y_pred == label) == 0

        if nan_strategy == "A":
            if score_fn == precision_score and label_not_predicted:
                assert (scores[label] == 0)
                scores[label] = np.nan
            elif score_fn == recall_score and label_missing:
                assert (scores[label] == 0)
                scores[label] = np.nan
            elif (score_fn in [f1_score, jaccard_score]) and (label_missing and label_not_predicted):
                assert (scores[label] == 0)
                scores[label] = np.nan
        elif nan_strategy == "B":
            if label_missing:
                assert (scores[label] == 0)
                scores[label] = np.nan
            elif score_fn == precision_score and label_not_predicted:
                assert (scores[label] == 0)
                scores[label] = np.nan
        else:  # nan_strategy == "C"
            if label_missing:
                if score_fn == recall_score:
                    assert (scores[label] == 0)
                    scores[label] = np.nan
                else:
                    assert (score_fn in [precision_score, f1_score, jaccard_score])
                    if label_not_predicted:
                        assert (scores[label] == 0)
                        scores[label] = 1
                    else:
                        assert (scores[label] == 0)
            elif score_fn == precision_score and label_not_predicted:
                assert (scores[label] == 0)
                scores[label] = np.nan

    return scores


def calc_conf_mat_metrics(confusion_matrix, nlabels):
    if confusion_matrix.dtype != float:
        confusion_matrix = confusion_matrix.astype(float)

    # Precision: TP / (TP + FP)
    # Recall: TP / (TP + FN)
    # F1: 2 * TP / (2 * TP + FP + FN)
    # Jaccard: TP / (TP + FP + FN)

    precision = np.zeros(nlabels, dtype=float)
    recall = np.zeros(nlabels, dtype=float)
    jaccard = np.zeros(nlabels, dtype=float)
    f1 = np.zeros(nlabels, dtype=float)

    for p in range(nlabels):
        if np.sum(confusion_matrix[:, p]) == 0:
            precision[p] = np.nan
        else:
            precision[p] = confusion_matrix[p, p] / (np.sum(confusion_matrix[:, p]))

        if np.sum(confusion_matrix[p, :]) == 0:
            recall[p] = np.nan
        else:
            recall[p] = confusion_matrix[p, p] / (np.sum(confusion_matrix[p, :]))

        if (np.sum(confusion_matrix[p, :]) + np.sum(confusion_matrix[:, p])) == 0:
            f1[p] = np.nan
        else:
            f1[p] = (2 * confusion_matrix[p, p]) / (np.sum(confusion_matrix[p, :]) + np.sum(confusion_matrix[:, p]))

            if not (np.isnan(precision[p]) or np.isnan(recall[p]) or (precision[p] + recall[p] == 0)):
                # sanity check
                f1_ = (2 * precision[p] * recall[p]) / (precision[p] + recall[p])
                assert (abs(f1[p] - f1_) < eps)

        if (np.sum(confusion_matrix[p, :]) + np.sum(confusion_matrix[:, p]) - confusion_matrix[p, p]) == 0:
            jaccard[p] = np.nan
        else:
            jaccard[p] = confusion_matrix[p, p] / (
                    np.sum(confusion_matrix[p, :]) + np.sum(confusion_matrix[:, p]) - confusion_matrix[p, p]
            )

    framewise_accuracy = 0
    for p in range(nlabels):
        framewise_accuracy += confusion_matrix[p, p]
    framewise_accuracy /= np.sum(confusion_matrix)

    return {
        "precision": precision,
        "recall": recall,
        "f1": f1,
        "jaccard": jaccard,
        "accuracy": framewise_accuracy
    }


# re-implementation based on https://github.com/YuemingJin/TMRNet/tree/main/code/eval/result/matlab-eval
def calc_relaxed_video_metrics(gtLabelID, predLabelID, labels, fps=1, w=10,
                               compare_to_orig=True, reproduce_bug=True, clip_results=True):
    if compare_to_orig is True:
        assert (w == 10)
        assert (reproduce_bug is True)
        octave.addpath(OCTAVE_PATH)

    oriT = w * fps  # relaxed boundary

    jaccard, prec, rec = [], [], []
    diff = predLabelID - gtLabelID

    phase_labels, phase_borders = get_phase_segments(gtLabelID)  # instead of finding connected components

    # obtain the true positive with relaxed boundary
    for iPhase in phase_labels:
        phase_idx = phase_labels.index(iPhase)
        startIdx = phase_borders[phase_idx]
        endIdx = phase_borders[phase_idx + 1]

        currDiff = diff[startIdx:endIdx]

        # in the case where the phase is shorter than the relaxed boundary
        t = oriT
        if t > len(currDiff):
            t = len(currDiff)
            print('Very short phase {}'.format(iPhase))

        if reproduce_bug is True:

            # relaxed boundary
            """
            # includes *bug* to conform with Matlab implementation: 
            # create index by checking the last t array elements, then append zeros to index (when preprending zeros would be correct)
            """
            if iPhase == 3 or iPhase == 4:  # Gallbladder dissection and packaging might jump between two phases
                currDiff[np.concatenate((currDiff[:t] == -1, np.zeros(len(currDiff) - t, dtype=bool)), axis=0)] = 0  # late transition
                currDiff[np.concatenate((np.logical_or(currDiff[-t:] == 1, currDiff[-t:] == 2), np.zeros(len(currDiff) - t, dtype=bool)), axis=0)] = 0  # early transition
                # 3 can be preceded by 2; 4 can be preceded by 3
                # 3 can be succeeded by 4/ 5; 4 can be succeeded by 5/ 6
            elif iPhase == 5 or iPhase == 6:  # Gallbladder dissection might jump between two phases
                currDiff[np.concatenate((np.logical_or(currDiff[:t] == -1, currDiff[:t] == -2), np.zeros(len(currDiff) - t, dtype=bool)), axis=0)] = 0  # late transition
                currDiff[np.concatenate((np.logical_or(currDiff[-t:] == 1, currDiff[-t:] == 2), np.zeros(len(currDiff) - t, dtype=bool)), axis=0)] = 0  # early transition
                # 5 can be preceded by 3/ 4; 6 can be preceded by 4/ 5
                # 5 can be succeeded by 6
            else:  # general situation --> linear progress
                currDiff[np.concatenate((currDiff[:t] == -1, np.zeros(len(currDiff) - t, dtype=bool)), axis=0)] = 0  # late transition
                currDiff[np.concatenate((currDiff[-t:] == 1, np.zeros(len(currDiff) - t, dtype=bool)), axis=0)] = 0  # early transition
            # *** cases not covered by the above rules ***
            # 4 can be preceded by 5 and - in other cases - 5 can be succeeded by 4
            # 5 can be preceded by 6 and - in other cases - 6 can be succeeded by 5

        else:
            index = np.arange(len(currDiff))
            start_mask = index < t
            end_mask = index >= (len(currDiff) - t)

            # relaxed boundary
            if iPhase == 3 or iPhase == 4:  # Gallbladder dissection and packaging might jump between two phases
                currDiff[np.logical_and(start_mask, currDiff == -1)] = 0  # late transition
                currDiff[np.logical_and(end_mask, np.logical_or(currDiff == 1, currDiff == 2))] = 0  # early transition
            elif iPhase == 5 or iPhase == 6:  # Gallbladder dissection might jump between two phases
                currDiff[np.logical_and(start_mask, np.logical_or(currDiff == -1, currDiff == -2))] = 0  # late transition
                currDiff[np.logical_and(end_mask, np.logical_or(currDiff == 1, currDiff == 2))] = 0  # early transition
            else:  # general situation
                currDiff[np.logical_and(start_mask, currDiff == -1)] = 0  # late transition
                currDiff[np.logical_and(end_mask, currDiff == 1)] = 0  # early transition

        # updates to currDiff will affect diff directly

    # compute jaccard index, prec, and rec per phase
    for iPhase in labels:
        if iPhase not in phase_labels:
            # no iPhase in current ground truth, assigned NaN values
            # SHOULD be excluded in the computation of mean (use nanmean)
            jaccard.append(np.nan)
            prec.append(np.nan)
            rec.append(np.nan)
        else:
            iPUnion = np.logical_or(predLabelID == iPhase, gtLabelID == iPhase)  # TP + FP + FN
            tp = np.sum(diff[iPUnion] == 0)  # "relaxed" true positives
            assert (tp >= np.sum(np.logical_and(predLabelID == iPhase, gtLabelID == iPhase)))
            jaccard.append(tp / np.sum(iPUnion))

            # Compute prec and rec
            sumPred = np.sum(predLabelID == iPhase)  # TP + FP
            sumGT = np.sum(gtLabelID == iPhase)  # TP + FN
            if sumPred == 0:
                prec.append(np.nan)
            else:
                prec.append(tp / sumPred)
            assert (sumGT > 0)
            rec.append(tp / sumGT)

    # compute accuracy
    acc = np.sum(diff == 0) / len(gtLabelID)

    prec = np.asarray(prec)
    rec = np.asarray(rec)
    jaccard = np.asarray(jaccard)

    if compare_to_orig is True:  # compare to original matlab implementation
        jaccard_orig, prec_orig, rec_orig, acc_orig = octave.feval('Evaluate', gtLabelID + 1, predLabelID + 1, fps, nout=4)
        for reimpl, orig in [(jaccard, jaccard_orig), (prec, prec_orig), (rec, rec_orig)]:
            orig = orig.squeeze()
            orig[orig == np.inf] = np.nan  # catch deviations due to division by zero
            diff = np.abs(reimpl * 100 - orig)
            is_equal = np.logical_or(np.isnan(diff), diff < eps)
            assert (np.all(is_equal))
        assert (acc * 100 == acc_orig)

    if clip_results:
        prec = np.clip(prec, 0, 1)
        rec = np.clip(rec, 0, 1)
        jaccard = np.clip(jaccard, 0, 1)

    metrics = {}
    metrics['accuracy'] = acc
    metrics['precision'] = prec
    metrics['recall'] = rec
    metrics['jaccard'] = jaccard

    return metrics


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Generate evaluation report.")
    parser.add_argument("--eval_data", type=str, choices=["Cholec80", "AutoLaparo", "SAR-RARP50"], default="Cholec80",
                        help="The dataset used for evaluation.")
    parser.add_argument("--experiment", type=str, default="ResNet-LSTM_S-8", help="The experiment to evaluate.")
    parser.add_argument("--datasplit", type=str, default="40-40",
                        help="The data split to use. "
                             "For Cholec80, known data splits are 32-8-40, 40-40, 40-20-20, and 40-8-32. "
                             "For AutoLaparo, known data splits are 10-4-7 and 14-7. "
                             "For SAR-RARP50, known data splits are 35-5-10. ")
    parser.add_argument("--results_root", type=str, default="PhaseMetrics/results", help="Where to look for the predictions.")
    parser.add_argument("--data_root", type=str, default="??", help="Where to look for the folder with phase annotations.")
    parser.add_argument("--out_dir", type=str, default=None, help="Where to store the evaluation report. Optional.")
    parser.add_argument("--run", type=str, default=None, help="The experimental run to evaluate. Optional.")
    parser.add_argument("--fps", type=int, default=1,
                        help="Temporal resolution of the predictions. Should be set to 10 on SAR-RARP50.")

    args = parser.parse_args()

    if args.data_root == '??':
        print("Please specify the location of the evaluation data using the --data_root option.")
    else:
        report_results(args.experiment, args.datasplit, args.data_root, args.results_root,  args.run, args.out_dir,
                       eval_data=args.eval_data, fps=args.fps)
