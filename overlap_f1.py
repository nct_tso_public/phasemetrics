import numpy as np

from .edit_score import segment_labels, segment_intervals


def calc_overlap_f1(y_true, y_pred, nlabels, overlap=0.5):
    assert (0 < overlap <= 1)
    TP, FP, FN = overlap_(y_pred, y_true, nlabels, bg_class=None, overlap=overlap)

    if (2 * TP + FP + FN) > 0:
        f1 = 2 * TP / (2 * TP + FP + FN)
    else:
        f1 = 0

    return f1


"""
MIT License

Copyright (c) 2016 Colin Lea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


# from https://github.com/colincsl/TemporalConvolutionalNetworks/blob/master/code/metrics.py
def overlap_(p, y, n_classes, bg_class, overlap):
    true_intervals = np.array(segment_intervals(y))
    true_labels = segment_labels(y)
    pred_intervals = np.array(segment_intervals(p))
    pred_labels = segment_labels(p)

    # Remove background labels
    if bg_class is not None:
        true_intervals = true_intervals[true_labels != bg_class]
        true_labels = true_labels[true_labels != bg_class]
        pred_intervals = pred_intervals[pred_labels != bg_class]
        pred_labels = pred_labels[pred_labels != bg_class]

    n_true = true_labels.shape[0]
    n_pred = pred_labels.shape[0]

    # We keep track of the per-class TPs, and FPs.
    # In the end we just sum over them though.
    TP = np.zeros(n_classes, float)
    FP = np.zeros(n_classes, float)
    true_used = np.zeros(n_true, float)

    for j in range(n_pred):
        # Compute IoU against all others
        intersection = np.minimum(pred_intervals[j, 1], true_intervals[:, 1]) - np.maximum(pred_intervals[j, 0], true_intervals[:, 0])
        union = np.maximum(pred_intervals[j, 1], true_intervals[:, 1]) - np.minimum(pred_intervals[j, 0], true_intervals[:, 0])
        IoU = (intersection / union) * (pred_labels[j] == true_labels)

        # Get the best scoring segment
        idx = IoU.argmax()

        # If the IoU is high enough and the true segment isn't already used
        # Then it is a true positive. Otherwise is it a false positive.
        if IoU[idx] >= overlap and not true_used[idx]:
            TP[pred_labels[j]] += 1
            true_used[idx] = 1
        else:
            FP[pred_labels[j]] += 1

    TP = TP.sum()
    FP = FP.sum()
    # False negatives are any unused true segment (i.e. "miss")
    FN = n_true - true_used.sum()

    return TP, FP, FN
