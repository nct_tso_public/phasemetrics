import argparse
import numpy as np
import pandas as pd
import seaborn as sn

from ..eval import get_results
from ..utils import Cholec80


def plot_overall_confusion_matrix(datasplit, data_root, normalize="row", include_sums=False, w=6.5, h=4.0, store_as="svg"):
    assert (normalize in ["none", "row", "column"])
    assert (store_as in ["svg", "pgf"])

    if store_as == "pgf":
        import matplotlib
        matplotlib.use("pgf")
        matplotlib.rcParams.update({
            "pgf.texsystem": "pdflatex",
            'font.family': 'serif',
            'text.usetex': True,
            'pgf.rcfonts': False,
        })
    import matplotlib.pyplot as plt

    _, mat = get_results("ResNet-LSTM_S-8", datasplit, data_root, results_root="PhaseMetrics/results",
                         return_conf_mat=True)

    cmap = sn.color_palette("Greys", as_cmap=True)
    labels = list(range(Cholec80.num_phases))
    sum_symbol = 'Σ' if store_as == "svg" else '$\Sigma$'

    if normalize == "none":
        mat_df = pd.DataFrame(mat, index=labels, columns=labels)
        sn.heatmap(mat_df, annot=True, cmap=cmap, cbar=False, square=True, fmt='d', annot_kws={"size": 10})
    else:
        mat = mat.astype(float)
        if normalize == "row":  # normalize per row
            for i in range(mat.shape[0]):
                mat[i, :] = mat[i, :] / np.sum(mat[i, :])
        else:  # normalize == "column"
            for i in range(mat.shape[0]):
                mat[:, i] = mat[:, i] / np.sum(mat[:, i])
        col_sums = np.sum(mat, axis=0)
        row_sums = np.sum(mat, axis=1)
        if normalize == "row":
            assert (np.allclose(row_sums, np.ones((mat.shape[0]), dtype=float)))
        else:
            assert (np.allclose(col_sums, np.ones((mat.shape[0]), dtype=float)))

        mat_df = pd.DataFrame(mat, index=labels, columns=labels)
        if include_sums is True:
            mat_df[sum_symbol] = row_sums
            mat_df = pd.concat([mat_df, pd.DataFrame([col_sums], index=[sum_symbol], columns=labels)], ignore_index=False)
        sn.heatmap(mat_df, annot=True, cmap=cmap, vmin=0, vmax=1, cbar=False, square=True, fmt='.2f', annot_kws={"size": 10})

    ax = plt.gca()
    ax.xaxis.tick_top()
    ax.xaxis.set_label_position('top')
    plt.yticks(rotation=0)
    plt.ylabel("Actual labels")
    plt.xlabel("Predicted labels")
    plt.gcf().set_size_inches(w, h)

    if store_as == "svg":
        plt.savefig('PhaseMetrics/paper/confusion_matrix_norm-{}.svg'.format(normalize))
    else:
        plt.savefig('PhaseMetrics/paper/confusion_matrix_norm-{}.pgf'.format(normalize))
    plt.clf()


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Script to visualize the overall confusion matrix.")
    parser.add_argument("--datasplit", type=str, choices=["32-8-40", "40-40"], default="32-8-40",
                        help="The data split to use.")
    parser.add_argument("--filetype", type=str, choices=["svg", "pgf"], default="svg",
                        help="File type for storing the plot.")
    parser.add_argument("--data_root", type=str, default="??",
                        help="Where to look for the Cholec80 phase annotations.")

    args = parser.parse_args()
    if args.data_root == '??':
        print("Please specify the location of the Cholec80 data directory using the --data_root option.")
    else:
        for normalize in ["column", "row"]:
            plot_overall_confusion_matrix(args.datasplit, args.data_root, normalize,  w=6.5, h=4.0, store_as=args.filetype)
