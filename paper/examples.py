import numpy as np
from ..eval import calc_relaxed_video_metrics


def averaging_example():
    x = np.array([
        [0.1, 0.2, 0.3],
        [0.1, 0.2, np.nan],
        [0.1, np.nan, 0.3]
    ])
    print("Calculating averages for example...")
    print("x = {}".format(x))
    print("********************************************************")
    print("phases first: {:.4f}".format(np.mean(np.nanmean(x, axis=1))))
    print("videos first: {:.4f}".format(np.mean(np.nanmean(x, axis=0))))
    print("all at once: {:.4f}".format(np.nanmean(x)))


def relaxed_example():
    predicted = [3, 5, 4, 4, 3, 3, 3, 4, 6, 3, 4, 4, 6, 5, 6, 5, 4, 6]
    target = [3, 3, 3, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6]
    labels = [3, 4, 5, 6]

    print("Calculating relaxed metrics for example...")
    print("True: ", target)
    print("Pred: ", predicted)
    print("Phases:\t", labels)

    for bug in [False, True]:
        print("********************************************************")
        print("Reproduce bug? {}".format(bug))
        relaxed_metrics = calc_relaxed_video_metrics(np.asarray(target), np.asarray(predicted), labels, w=2,
                                                     compare_to_orig=False, reproduce_bug=bug, clip_results=False)

        print("jaccard\t", relaxed_metrics["jaccard"])
        print("precision", relaxed_metrics["precision"])
        print("recall\t", relaxed_metrics["recall"])


if __name__ == '__main__':
    relaxed_example()
    print("\n")
    averaging_example()
