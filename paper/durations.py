import argparse
import numpy as np

from ..utils import Cholec80, get_phase_segments

from collections import namedtuple
Video = namedtuple('Video', 'phase_labels phase_borders')


def check_missing(data_root, datasplit="32-8-40"):
    print("***** datasplit: {} *****".format(datasplit))
    for subset in ['train', 'val', 'test']:
        video_ids = Cholec80.get_datasplit(datasplit)[subset]
        targets = Cholec80.get_targets(video_ids, data_root=data_root, fps=1)

        video_list = []
        for video_id in video_ids:
            video_list.append(Video(*get_phase_segments(targets[video_id])))

        nphases = Cholec80.num_phases
        counts = np.zeros(nphases)
        for video in video_list:
            for phase in range(nphases):
                if phase in video.phase_labels:
                    counts[phase] += 1
        print("counts", counts)

        missing_phases = []
        for phase in range(nphases):
            if counts[phase] < len(video_list):
                missing_phases.append(phase)
        print("{}: Found missing phases: {}".format(subset, missing_phases))


def get_phase_durations(data_root):
    video_ids = ["{:02d}".format(i) for i in range(1, 81)]
    targets = Cholec80.get_targets(video_ids, data_root=data_root, fps=1)

    video_list = []
    for video_id in video_ids:
        video_list.append(Video(*get_phase_segments(targets[video_id])))

    nphases = Cholec80.num_phases
    durations = [[] for _ in range(nphases)]
    for video in video_list:
        for phase in range(nphases):
            if phase in video.phase_labels:
                i = video.phase_labels.index(phase)
                assert (video.phase_labels[i] == phase)
                duration = video.phase_borders[i + 1] - video.phase_borders[i]
                durations[phase].append(duration)
            else:
                pass
                # durations[phase].append(np.nan)

    # sanity check
    for phase in range(nphases):
        p_durations = np.asarray(durations[phase]) / 60
        print("phase {} duration [min]: {:.2f}; {:.2f} +/- {:.2f} [{:.2f}, {:.2f}]".
              format(phase,
                     np.median(p_durations), np.mean(p_durations), np.std(p_durations, ddof=1),
                     np.min(p_durations), np.max(p_durations))
              )

    return durations


def plot_phase_durations(data_root, w=6.5, h=4.0, logscale=True, show_datapoints=True, store_as="svg"):
    assert (store_as in ["svg", "pgf"])

    if store_as == "pgf":
        import matplotlib
        matplotlib.use("pgf")
        matplotlib.rcParams.update({
            "pgf.texsystem": "pdflatex",
            'font.family': 'serif',
            'text.usetex': True,
            'pgf.rcfonts': False,
        })
    import matplotlib.pyplot as plt

    np.random.seed(42)

    nphases = Cholec80.num_phases
    data = get_phase_durations(data_root)
    x_labels = [i for i in range(nphases)]
    xs = [np.random.normal(i + 1, 0.075, len(data[i])) for i in range(nphases)]

    plt.boxplot(data, labels=x_labels, medianprops={"color": "black"})
    if show_datapoints is True:
        for x, val in zip(xs, data):
            plt.scatter(x, val, alpha=0.5, color="gray", s=10)

    plt.xlabel("Phase")
    plt.ylabel("Duration")
    if logscale is True:
        plt.yscale("log")
        plt.yticks(ticks=[30, 60, 120, 240, 480, 960, 1920, 3840],
                   labels=["30 s", "60 s", "2 min", "4 min", "8 min", "16 min", "32 min", "64 min"])
    else:
        plt.yticks(ticks=[60, 240, 480, 960, 1920, 3840],
                   labels=["1 min", "4 min", "8 min", "16 min", "32 min", "64 min"])

    plt.gcf().set_size_inches(w, h)

    if store_as == "svg":
        plt.savefig('PhaseMetrics/paper/phase_durations.svg')
    else:
        plt.savefig('PhaseMetrics/paper/phase_durations.pgf')
    plt.clf()


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Script to plot the phase durations over all Cholec80 videos.")
    parser.add_argument("--yscaling", type=str, choices=["linear", "log"], default="linear",
                        help="Scaling of the y-axis.")
    parser.add_argument("--filetype", type=str, choices=["svg", "pgf"], default="svg",
                        help="File type for storing the plot.")
    parser.add_argument("--data_root", type=str, default="??",
                        help="Where to look for the Cholec80 phase annotations.")

    args = parser.parse_args()
    if args.data_root == '??':
        print("Please specify the location of the Cholec80 data directory using the --data_root option.")
    else:
        if args.yscaling == "log":
            plot_phase_durations(
                args.data_root, w=6.5, h=4.0, logscale=True, show_datapoints=True, store_as=args.filetype
            )
        else:
            plot_phase_durations(
                args.data_root, w=6.5, h=4.0, logscale=False, show_datapoints=False, store_as=args.filetype
            )
