This subfolder contains additional scripts to reproduce the figures and examples in the associated metrics paper.

For some of the scripts, additional python packages are required:

- matplotlib
- seaborn
- pandas 
 