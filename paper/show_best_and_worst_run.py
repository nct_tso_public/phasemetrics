import argparse
from ..eval import report_results


def main(datasplit, data_root):
    results = []
    for run in ["0", "1", "2", "3", "4"]:
        report = report_results("ResNet-LSTM_S-8", datasplit, data_root, "PhaseMetrics/results", run=run)
        results.append((run, report["jaccard"]['B']["mean"], report["accuracy"]["mean"]))

    results = sorted(results, key=lambda t: t[2])
    results = sorted(results, key=lambda t: t[1])

    print(results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--datasplit", type=str, default="32-8-40", help="The data split to use.")
    parser.add_argument("--data_root", type=str, default="??", help="Where to look for the Cholec80 phase annotations.")

    args = parser.parse_args()

    if args.data_root == '??':
        print("Please specify the location of the Cholec80 data directory using the --data_root option.")
    else:
        main(args.datasplit, args.data_root)
