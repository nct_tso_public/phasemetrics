import argparse
import numpy as np

from ..eval import get_results
from ..utils import Cholec80


def plot_jaccard_per_phase(datasplit, data_root, w=6.5, h=4.0, store_as="svg"):
    assert (store_as in ["svg", "pgf"])

    if store_as == "pgf":
        import matplotlib
        matplotlib.use("pgf")
        matplotlib.rcParams.update({
            "pgf.texsystem": "pdflatex",
            'font.family': 'serif',
            'text.usetex': True,
            'pgf.rcfonts': False,
        })
    import matplotlib.pyplot as plt

    test_ids = Cholec80.get_datasplit(datasplit)['test']
    experiment_results = get_results("ResNet-LSTM_S-8", datasplit, data_root, "PhaseMetrics/results")
    nruns = len(experiment_results)
    assert (nruns > 0)

    # collect scores to plot (without NaNs)
    jaccard_scores = []
    x_labels = []
    for phase in range(Cholec80.num_phases):
        if phase != 5:
            jaccard_scores.append([
                experiment_results[run][video_id]['jaccard']['A'][phase] for video_id in test_ids for run in range(nruns)
                if not np.isnan(experiment_results[run][video_id]['jaccard']['A'][phase])
            ])
            x_labels.append("Phase{}{}".format(' ' if store_as == "svg" else '\,', phase))
        else:
            jaccard_scores.append([
                experiment_results[run][video_id]['jaccard']['A'][phase] for video_id in test_ids for run in range(nruns)
                if not np.isnan(experiment_results[run][video_id]['jaccard']['A'][phase])
            ])
            x_labels.append("Phase{}5\n{}".format(' ' if store_as == "svg" else '\,',
                                                  "Strategy A" if store_as == "svg" else "$\mathsf{Strategy\,A}$"))
            jaccard_scores.append([
                experiment_results[run][video_id]['jaccard']['B'][phase] for video_id in test_ids for run in range(nruns)
                if not np.isnan(experiment_results[run][video_id]['jaccard']['B'][phase])
            ])
            x_labels.append("Phase{}5\n{}".format(' ' if store_as == "svg" else '\,',
                                                  "Strategy B" if store_as == "svg" else "$\mathsf{Strategy\,B}$"))
    # print([len(jaccard_scores[i]) for i in range(len(jaccard_scores))])

    np.random.seed(42)
    x_positions = list(range(len(jaccard_scores)))
    jitter = 0.1
    xs = [np.random.normal(x_positions[i], jitter, len(jaccard_scores[i])) for i in range(len(jaccard_scores))]

    # gray background for Phase 5
    offset = 0.5
    plt.axvspan(5 - offset, 6 + offset, facecolor='gray', alpha=0.15)

    # draw boxes and data points
    plt.boxplot(jaccard_scores, positions=x_positions, labels=x_labels, medianprops={"color": "black"})
    for data_points, x in zip(jaccard_scores, xs):
        plt.scatter(x, data_points, alpha=0.25, color="gray", s=15)

    plt.ylabel("Jaccard" if store_as == "svg" else "$\mathsf{Jaccard}$")
    plt.ylim(-0.1, 1.1)
    plt.xticks(x_positions, labels=x_labels, rotation=-30, va="top")
    plt.subplots_adjust(bottom=0.2)
    plt.gcf().set_size_inches(w, h)

    plt.savefig("PhaseMetrics/paper/jaccard_per_phase.{}".format("svg" if store_as == "svg" else "pgf"))
    plt.clf()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "Script to visualize the phase-wise Jaccard scores for all test videos and experimental runs."
    )
    parser.add_argument("--datasplit", type=str, choices=["32-8-40", "40-40"], default="32-8-40",
                        help="The data split to use.")
    parser.add_argument("--filetype", type=str, choices=["svg", "pgf"], default="svg",
                        help="File type for storing the plot.")
    parser.add_argument("--data_root", type=str, default="??",
                        help="Where to look for the Cholec80 phase annotations.")

    args = parser.parse_args()
    if args.data_root == '??':
        print("Please specify the location of the Cholec80 data directory using the --data_root option.")
    else:
        plot_jaccard_per_phase(args.datasplit, args.data_root, w=6.5, h=4.0, store_as=args.filetype)
