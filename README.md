# Evaluation Metrics for Surgical Phase Recognition

This repository provides scripts and functions to compute the most common evaluation metrics for the task of 
surgical phase recognition on the [Cholec80](http://camma.u-strasbg.fr/datasets) benchmark.

These metrics include:

- Accuracy
- Precision, Recall, Jaccard, and F1
- A Python implementation of the **relaxed** evaluation metrics, which were used in many recent publications, 
such as [TMRNet](https://github.com/YuemingJin/TMRNet/tree/main/code/eval/result/matlab-eval).

## Updates

- Jan 2025: Added support for the 
[SAR-RARP50](https://rdr.ucl.ac.uk/projects/SAR-RARP50_Segmentation_of_surgical_instrumentation_and_Action_Recognition_on_Robot-Assisted_Radical_Prostatectomy_Challenge/191091) dataset. 

  Here, we assume a train-val-test split that uses the first 35 videos for training, the following 5 for validation,
and the last 10 videos for testing. 
To evaluate your predictions on SAR-RARP50, specify `--eval_data "SAR-RARP50" --datasplit "35-5-10" --fps 10`. 
Further, we assume that `--data_root` points to the directory that contains SAR-RARP50's `video_*` folders.

- Oct 2023: Compute **edit score** and **F1@k** (previously proposed [here](https://doi.org/10.1007/978-3-319-46487-9_3) and 
implemented [here](https://github.com/colincsl/TemporalConvolutionalNetworks)) as additional video-wise evaluation metrics.
- Oct 2023: Added support for the [AutoLaparo](https://autolaparo.github.io/) dataset. 
Relaxed metrics will only be computed on Cholec80.

  To evaluate your predictions on AutoLaparo, specify `--eval_data "AutoLaparo" --datasplit "10-4-7"`. Further, we assume
that predicted phase labels range from 0 to 6 (instead of 1 to 7) and that `--data_root` points to the directory that 
contains AutoLaparo's `labels` folder. See below and [here](#data-preparation) for more information on how to run `eval.py`.

## Reference paper

A detailed definition of all implemented evaluation metrics along with an overview of previously published 
evaluation results on the Cholec80 dataset is provided in our [technical report](https://arxiv.org/abs/2305.13961).

## Implementation details

- Python code with few dependencies.
- Input and output files are simple, human-readable .yaml files.
- For each metric, the script will calculate mean, standard deviation over videos, standard deviation over phases, 
if applicable, and standard deviation over experimental runs, if the predictions of several experimental runs are provided.
- Implementation is based on numpy and [sklearn.metrics](https://scikit-learn.org/stable/index.html).
- Custom implementation of handling undefined values when calculating phase-wise video-wise metrics
(Precision, Recall, Jaccard, or F1)
	- Strategy A: Set undefined values to NaN and exclude them from following calculations 
(by using, e.g., np.nanmean and np.nanstd)
	- Strategy B: Similar to A. Additionally, if a phase is missing in the video annotation, set the Precision, Jaccard, 
and F1 scores for this video and the missing phase to NaN even if they may *not* be undefined.
- Despite some shortcomings of the original scripts for calculating the **relaxed** evaluation metrics 
(see our [technical report](https://arxiv.org/abs/2305.13961) for a detailed discussion), our code matches the original implementation 1:1.
- If `Octave` and `oct2py` (see [below](#installing-octave-and-oct2py-optional)) are installed on the system, 
the computed relaxed evaluation scores will be automatically compared to the scores calculated with the original MATLAB script (see directory `matlab-eval`).

## Format of the evaluation report

Based on the provided phase predictions, the script `eval.py` generates an evaluation report, `eval.yaml`. 
This .yaml file basically presents a Python dictionary that stores all evaluation metrics in a structured way. 
All values range between 0 and 1 because we do not multiply by 100 %.

In general, the report contains one entry for each evaluation metric, which comprises the overall mean at key `'mean'`, 
standard deviation over videos at key `'std_V'`, standard deviation over phases, if applicable, at key `'std_P'`, 
and standard deviation over experimental runs, if predictions for multiple runs are provided, at key `'std_R'`.
Furthermore, the phase-wise video-wise metrics are reported for both strategies for handling missing phases 
(`'A'` or `'B'`) and also for each phase `p` individually, where `0 <= p < 7`. For these metrics, we additionally 
report the mean over phase-wise means at key  `'mean_P'`.

To conform with the original MATLAB script, we report the mean and standard deviation over videos for relaxed accuracy 
and the mean over phase-wise means and the standard deviation over phases for relaxed jaccard, relaxed precision, and relaxed recall.

The following table summarizes how the evaluation metrics, defined in our [technical report](https://arxiv.org/abs/2305.13961), 
correspond to the values in the generated evaluation report:

<img src="resources/phase_metrics.png" alt="You should see a list of the reported evaluation metrics here." width="75%" height="75%">

## Citation

If the provided code is useful for your research, please consider citing:

```
@article{funke2023metrics,
  title={Metrics Matter in Surgical Phase Recognition},
  author={Funke, Isabel and Rivoir, Dominik and Speidel, Stefanie},
  journal={arXiv preprint},
  doi={10.48550/arXiv.2305.13961},
  url={https://arxiv.org/abs/2305.13961},
  year={2023}
}

```

## Code usage

### Getting started

Clone the repository:
```
cd <code_root>
git clone https://gitlab.com/nct_tso_public/phasemetrics.git -b master PhaseMetrics
```
The expected folder structure is
```
<code_root>
	PhaseMetrics
		matlab-eval
			...
		results
			...
		eval.py
		...
```
Here `<code_root>` is the file path to the folder that contains the `PhaseMetrics` directory.

### Dependencies

- numpy
- scikit-learn
- pyyaml
- Optional: pandas
- Optional: oct2py

### Installing Octave and oct2py (optional)
This step is required only if you want that the computed relaxed evaluation scores are compared to the scores calculated 
with the original MATLAB script. In addition, you can directly run the original MATLAB script when you have Octave installed.

- Install octave
    ```
    sudo apt-get install octave octave-doc gnuplot liboctave-dev
    ```

- Install packages in Octave (required to run the MATLAB script)

    Start the Octave shell:
    ```
    octave
    ```
    Then run
    ```
    pkg install -forge io
    pkg install "https://downloads.sourceforge.net/project/octave/Octave%20Forge%20Packages/Individual%20Package%20Releases/statistics-1.4.3.tar.gz"
    pkg install -forge image
    exit % leave the Octave shell
    ```

- Install [oct2py](https://pypi.org/project/oct2py/) to call and excute Octave code in a Python script
    ```
    pip install oct2py
    ```

### Data preparation

- Download the [Cholec80 dataset](http://camma.u-strasbg.fr/datasets), unzip it and store it at some location (`data_root`). 
The expected folder structure looks like this:
    ```
    <data_root>
        phase_annotations
            video01-phase.txt
            video02-phase.txt
            ...
        ...
    ```
- The script `eval.py` expects to find the predictions obtained from several experimental runs for a method / experiment  
in .yaml files named `predictions.yaml`, which are stored in the following folder structure:
    ```
    <results_root>
        <experiment>
            <datasplit>
                <run_0>
                    predictions.yaml
                <run_1>
                    predictions.yaml
                ...
                <run_m>
                    predictions.yaml
    ```
    Here, `datasplit` is either `40-40`, `32-8-40`, or `40-8-32`. 
The names for the experiment and the run / repetition folders can be chosen arbitrarily. 
Note that at least one run subdirectory needs to be found, even if only one single experimental run was performed.

Each file `predictions.yaml` is a dictionary that stores, for each video in the test set, the sequence of predicted 
phase labels (integers between 0 and 6) as a list. The key for the video number x is `"{:02d}".format(x)`, where `1 <= x <= 80`. 
In addition, information about the prediction frequency (usually 1 frame per second) needs to be stored at key `"fps"`. 
Using pyyaml, such a .yaml file can be easily generated in Python:

```
import yaml

predictions = {
	"41": [0, 0, 0, 1, 1, 1, ...],
	"42": [1, 1, 1, 1, 1, 1, ...],
	"43": [0, 1, 0, 1, 0, 1, ...],
	...
}
predictions["fps"] = 1
yaml.safe_dump(predictions, stream=open("predictions.yaml", 'w'), default_flow_style=True)
```
If you currently have no prediction results at hand, you can test the evaluation code using the provided example.


### Example
We provide `predictions.yaml` files for a baseline experiment, where we trained a ResNet-LSTM model end to end on short 
video sequences of 8 frames (thus covering 8 seconds). For both data splits, `40-40` and `32-8-40`, the experiment was 
repeated five times using different random seeds. The predictions can be found in the `results` directory.

### Running the script
To compute the evaluation results given **all** predictions for the example experiment "ResNet-LSTM_S-8" and datasplit "40-40", run
```
cd <code_root>
python -m PhaseMetrics.eval --experiment "ResNet-LSTM_S-8" --datasplit "40-40" --results_root "PhaseMetrics/results" --data_root <data_root>
```
This will generate an [evaluation report](#format-of-the-evaluation-report) stored at `results/ResNet-LSTM_S-8/40-40/eval.yaml`.

## Comparison to original MATLAB script (requires pandas and Octave installation)

For comparison, individual experimental runs can also be evaluated with the original MATLAB script.

To do this for run 0 of the example experiment "ResNet-LSTM_S-8" and datasplit "40-40", you can follow these steps:

- Convert the `/results/ResNet-LSTM_S-8/40-40/0/predictions.yaml` file to the text files that are expected by the 
MATLAB script (requires the `pandas` package):
    ```
    cd <code_root>
    python -m PhaseMetrics.matlab-eval.convert_yaml_to_csv --experiment "ResNet-LSTM_S-8" --datasplit "40-40" --results_root "PhaseMetrics/results" --data_root <data_root> --run 0
    ```
    The script will create the folders `matlab-eval/gt-phase` and `matlab-eval/phase`, where `gt-phase` contains 
text files with ground truth annotations for every test video and `phase` contains the text files (one per test video) 
that correspond to the predictions in `/results/ResNet-LSTM_S-8/40-40/0/predictions.yaml`.

- Run the MATLAB script
    ```
    cd <code_root>/PhaseMetrics/matlab-eval
    octave Main.m
    ```
    or, to run the original, unmodified version,
    ```
    cd <code_root>/PhaseMetrics/matlab-eval/TMRNet
    octave Main.m
    ```
    The results will be printed to the screen.

- Get the evaluation report for run 0 of experiment "ResNet-LSTM_S-8", datasplit "40-40":

    ```
    cd <code_root>
    python -m PhaseMetrics.eval --experiment "ResNet-LSTM_S-8" --datasplit "40-40" --results_root "PhaseMetrics/results" --data_root <data_root> --run 0
    ```
  The evaluation report will be stored at `/results/ResNet-LSTM_S-8/40-40/0/eval.yaml`. 
The numbers reported for the relaxed evaluation metrics should equal the numbers printed by the MATLAB / Octave script.
 
