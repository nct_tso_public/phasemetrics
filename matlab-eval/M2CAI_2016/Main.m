close all; clear all;

phaseGroundTruths = {'train_dataset/workflow_video_02.txt', ...
    'train_dataset/workflow_video_04.txt'};

phases = {'TrocarPlacement', 'Preparation',  'CalotTriangleDissection', ...
    'ClippingCutting', 'GallbladderDissection',  'GallbladderPackaging', 'CleaningCoagulation', ...
    'GallbladderRetraction'};

fps = 25;

for i = 1:length(phaseGroundTruths)
    
    phaseGroundTruth = phaseGroundTruths{i};
    predFile = [phaseGroundTruth(1:end-4) '_pred.txt'];
    
    [gt] = ReadPhaseLabel(phaseGroundTruth);
    [pred] = ReadPhaseLabel(predFile);
    
    if(size(gt{1}, 1) ~= size(pred{1},1) || size(gt{2}, 1) ~= size(pred{2},1))
        error(['ERROR:' ground_truth_file '\nGround truth and prediction have different sizes']);
    end
    
    if(~isempty(find(gt{1} ~= pred{1})))
        error(['ERROR: ' ground_truth_file '\nThe frame index in ground truth and prediction is not equal']);
    end
    
    % reassigning the phase labels to numbers
    gtLabelID = [];
    predLabelID = [];
    for j = 1:length(phases)
        gtLabelID(find(strcmp(phases{j}, gt{2}))) = j;
        predLabelID(find(strcmp(phases{j}, pred{2}))) = j;
    end
    
    % compute jaccard index, precision, recall, and the accuracy
    [jaccard(:,i), prec(:,i), rec(:,i), acc(i)] = Evaluate(gtLabelID, predLabelID, fps);
    
end

% Compute means and stds
meanJaccPerPhase = nanmean(jaccard, 2);
meanJacc = mean(meanJaccPerPhase);
stdJacc = std(meanJaccPerPhase);

meanPrecPerPhase = nanmean(prec, 2);
meanPrec = mean(meanPrecPerPhase);
stdPrec = std(meanPrecPerPhase);

meanRecPerPhase = nanmean(rec, 2);
meanRec = mean(meanRecPerPhase);
stdRec = std(meanRecPerPhase);

meanAcc = mean(acc);
stdAcc = std(acc);

% Display results
disp('================================================');
disp([sprintf('%25s', 'Phase') '|' sprintf('%6s', 'Jacc') '|'...
    sprintf('%6s', 'Prec') '|' sprintf('%6s', 'Rec') '|']);
disp('================================================');
for iPhase = 1:length(phases)
    disp([sprintf('%25s', phases{iPhase}) '|' sprintf('%6.2f', meanJaccPerPhase(iPhase)) '|' ...
        sprintf('%6.2f', meanPrecPerPhase(iPhase)) '|' sprintf('%6.2f', meanRecPerPhase(iPhase)) '|']);
    disp('---------------------------------------------');
end
disp('================================================');

disp(['Mean jaccard: ' sprintf('%5.2f', meanJacc) ' +- ' sprintf('%5.2f', stdJacc)]);
disp(['Mean accuracy: ' sprintf('%5.2f', meanAcc) ' +- ' sprintf('%5.2f', stdAcc)]);
