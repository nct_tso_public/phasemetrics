function [ meanJacc, stdJacc, meanAcc, stdAcc,  meanPrec, stdPrec, meanRec, stdRec, meanJaccPerPhase, meanPrecPerPhase, meanRecPerPhase ] = Summarize(acc, jaccard, prec, rec)

pkg load statistics;

accPerVideo= acc;

% Compute means and stds
index = find(jaccard>100);
jaccard(index)=100;
meanJaccPerPhase = nanmean(jaccard, 2);  % phase-wise means
meanJaccPerVideo = nanmean(jaccard, 1);  % macro average, per video
meanJacc = mean(meanJaccPerPhase);  % overall mean
stdJacc = std(meanJaccPerPhase);  % variation over phases
for h = 1:7
    jaccphase = jaccard(h,:);
    meanjaccphase(h) = nanmean(jaccphase);  % phase-wise means
    stdjaccphase(h) = nanstd(jaccphase);  % phase-wise std (over videos)
end

index = find(prec>100);
prec(index)=100;
meanPrecPerPhase = nanmean(prec, 2);
meanPrecPerVideo = nanmean(prec, 1);
meanPrec = nanmean(meanPrecPerPhase);
stdPrec = nanstd(meanPrecPerPhase);
for h = 1:7
    precphase = prec(h,:);
    meanprecphase(h) = nanmean(precphase);
    stdprecphase(h) = nanstd(precphase);
end

index = find(rec>100);
rec(index)=100;
meanRecPerPhase = nanmean(rec, 2);
meanRecPerVideo = nanmean(rec, 1);
meanRec = mean(meanRecPerPhase);
stdRec = std(meanRecPerPhase);
for h = 1:7
    recphase = rec(h,:);
    meanrecphase(h) = nanmean(recphase);
    stdrecphase(h) = nanstd(recphase);
end

% mean and std over videos
meanAcc = mean(acc);
stdAcc = std(acc);

end

