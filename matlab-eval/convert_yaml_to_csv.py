import argparse
import os
import yaml
import numpy as np
import pandas as pd

from ..utils import Cholec80


def main(data_root, experiment="ResNet-LSTM_S-8", datasplit="40-40", run='0', results_root="./results",
         fps=1, overwrite_targets=False):
    """Create the text files required to run the MATLAB scripts.

    Text files will be created in matlab-eval/gt-phase (for the targets) and matlab-eval/phase (for the predictions).
    Predictions will be read from <results_root>/<experiment>/<datasplit>/<run>/predictions.yaml.

    Parameters
    ----------
    data_root: str
        Where to look for the Cholec80 data.
    experiment : str
        The experiment to use.
    datasplit : str
        The data split to use.
    run: str
        The experimental run to use.
    results_root: str, optional
        Where to look for the predictions.
    fps: int, optional
        The frequency (in frames per second) at which predictions were made. Defaults to 1.
    overwrite_targets: str, optional
        If True, existing text files in matlab-eval/gt-phase will be overwritten. Otherwise, they will be kept for reuse.
    """
    pred_out = "PhaseMetrics/matlab-eval/phase"
    target_out = "PhaseMetrics/matlab-eval/gt-phase"
    if not os.path.exists(pred_out):
        os.mkdir(pred_out)
    if not os.path.exists(target_out):
        os.mkdir(target_out)

    sep = ' '

    test_ids = Cholec80.get_datasplit(datasplit)['test']

    # check whether targets have been written already
    target_files = [f for f in os.listdir(target_out) if f.endswith('-phase.txt')]
    if len(target_files) == len(test_ids) and overwrite_targets is False:
        print("Found {} files in '{}'. Will not overwrite targets.".format(len(target_files), target_out))
    else:
        for f in target_files:
            os.remove(os.path.join(target_out, f))

        targets = Cholec80.get_targets(test_ids, data_root, fps)
        for video_id in test_ids:
            labels = np.asarray(targets[video_id])
            labels = pd.DataFrame(labels, columns=['Phase'])
            labels.to_csv(os.path.join(target_out, 'video{}-phase.txt'.format(video_id)),
                          index=True, index_label='Frame', sep=sep)

    # remove old files in pred_out
    pred_files = [f for f in os.listdir(pred_out) if f.endswith('-phase.txt')]
    for f in pred_files:
        os.remove(os.path.join(pred_out, f))

    predictions_file = os.path.join(results_root, experiment, datasplit, run, "predictions.yaml")
    if not os.path.exists(predictions_file):
        print("Could not find file '{}'".format(predictions_file))
        return
    print("Converting {}...".format(predictions_file))
    predictions_dict = yaml.safe_load(open(predictions_file))
    for video_id in test_ids:
        predictions = np.asarray(predictions_dict[video_id])
        predictions = pd.DataFrame(predictions, columns=['Phase'])
        predictions.to_csv(os.path.join(pred_out, 'video{}-phase.txt'.format(video_id)),
                           index=True, index_label='Frame', sep=sep)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Create the text files required to run the MATLAB scripts.")
    parser.add_argument("--experiment", type=str, default="ResNet-LSTM_S-8", help="The experiment to use.")
    parser.add_argument("--datasplit", type=str, default="40-40", help="The data split to use.")
    parser.add_argument("--run", type=str, default='0', help="The experimental run to use.")
    parser.add_argument("--results_root", type=str, default="PhaseMetrics/results", help="Where to look for the predictions.")
    parser.add_argument("--data_root", type=str, default="??", help="Where to look for the Cholec80 phase annotations.")

    args = parser.parse_args()

    if args.data_root == '??':
        print("Please specify the location of the Cholec80 data directory using the --data_root option.")
    else:
        main(args.data_root, args.experiment, args.datasplit,  args.run, args.results_root)
