MATLAB/ GNU Octave code for computing video-wise relaxed evaluation metrics
(accuracy, precision, recall, and Jaccard index).

The code is based on the evaluation script provided in the TMRNet repository,
to which the Trans-SVNet repository links as well.
For reference, see the `TMRNet` directory or
[github](https://github.com/YuemingJin/TMRNet/tree/main/code/eval/result/matlab-eval).

For completeness, the previous implementation found in the
SV-RCNet [repository](https://github.com/YuemingJin/SV-RCNet/tree/master/surgicalVideo/evaluation_Cholec80)
is provided in directory `SVRCNet`.

For reference, the original M2CAI implementation, which was created for the
[M2CAI 2016 challenge](http://camma.u-strasbg.fr/m2cai2016), can be found in directory `M2CAI_2016`.
