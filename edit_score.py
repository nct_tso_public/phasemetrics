import numpy as np

from .utils import get_phase_segments


def calc_edit_score(y_true, y_pred):
    segments_true = segment_labels(y_true).tolist()
    segments_pred = segment_labels(y_pred).tolist()

    levenshtein = levenstein_(segments_pred, segments_true)
    assert (levenshtein == levenstein_(segments_true, segments_pred))

    levenshtein_normalized = levenshtein / max(len(segments_pred), len(segments_true))
    assert (0 <= levenshtein_normalized <= 1)
    return 1 - levenshtein_normalized


"""
MIT License

Copyright (c) 2016 Colin Lea

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


# from https://github.com/colincsl/TemporalConvolutionalNetworks/blob/master/code/metrics.py
def levenstein_(p, y, norm=False):
    m_row = len(p)
    n_col = len(y)
    D = np.zeros([m_row + 1, n_col + 1], float)
    for i in range(m_row + 1):
        D[i, 0] = i
    for i in range(n_col + 1):
        D[0, i] = i

    for j in range(1, n_col + 1):
        for i in range(1, m_row + 1):
            if y[j - 1] == p[i - 1]:
                D[i, j] = D[i - 1, j - 1]
            else:
                D[i, j] = min(D[i - 1, j] + 1,
                              D[i, j - 1] + 1,
                              D[i - 1, j - 1] + 1)

    if norm:
        score = (1 - D[-1, -1] / max(m_row, n_col)) * 100
    else:
        score = D[-1, -1]

    return score


# from https://github.com/colincsl/TemporalConvolutionalNetworks/blob/master/code/utils.py
def segment_labels(Yi):
    idxs = [0] + (np.nonzero(np.diff(Yi))[0]+1).tolist() + [len(Yi)]
    Yi_split = np.array([Yi[idxs[i]] for i in range(len(idxs)-1)])

    # sanity check
    phase_labels, _ = get_phase_segments(Yi)
    assert (phase_labels == Yi_split.tolist())

    return Yi_split


def segment_intervals(Yi):
    idxs = [0] + (np.nonzero(np.diff(Yi))[0]+1).tolist() + [len(Yi)]
    intervals = [(idxs[i],idxs[i+1]) for i in range(len(idxs)-1)]

    # sanity check
    _, phase_borders = get_phase_segments(Yi)
    assert ([intervals[i][0] for i in range(len(intervals))] == phase_borders[:-1])
    assert ([intervals[i][1] for i in range(len(intervals))] == phase_borders[1:])

    return intervals
